﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.TypeDocument
{
    public class TypeDocumentModel : ModelBase
    {
        public int Id { get; set; }
        public string TypeDocumentName { get; set; }
        public string TypeDocumentCode { get; set; }
    }
}
