﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Model.Class;
using Model.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Class
{
    public interface IClassRepository
    {
        Response<string> CreateClass(ClassModel model);
        List<SelectListItem> GetListClassForCombo();
    }
}
