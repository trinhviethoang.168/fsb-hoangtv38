﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Model.Requests;
using Model.Response;
using Model.TypeDocument;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.TypeDocument
{
    public interface ITypeDocumentRepository
    {
        TableResponse<TypeDocumentViewModel> GetListTypeDocument(SearchCompanyModel search);
        Response<string> CreateTypeDocument(TypeDocumentModel model);
        Response<string> DeleteTypeDocument(TypeDocumentModel model);
        Response<TypeDocumentModel> GetTypeDocumentById(TypeDocumentModel model);
        Response<string> UpdateTypeDocument(TypeDocumentModel model);
        List<SelectListItem> GetListTypeDocumentForCombo();
    }
}
