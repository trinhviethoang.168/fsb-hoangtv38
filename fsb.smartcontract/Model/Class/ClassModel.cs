﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Class
{
    public class ClassModel : ModelBase
    {
        public int Id { get; set; }
        public string ClassName { get; set; }
    }
}
